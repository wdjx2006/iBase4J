package org.ibase4j.provider.sys;

import org.ibase4j.core.base.BaseProvider;
import org.ibase4j.model.generator.SysRole;

public interface SysRoleProvider extends BaseProvider<SysRole> {

}
